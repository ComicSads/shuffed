package main

import (
	"bufio"
	"fmt"
	"gitlab.com/ComicSads/pick-a-line/lib"
	"math/rand"
	"os"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	f, err := os.Open(os.Args[1])
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening file '"+os.Args[1]+"'")
		os.Exit(1)
	}
	r := bufio.NewReader(f)
	line := rand.Intn(pal.LineCount(r)) + 1
	f.Seek(0, 0)
	s, err := pal.ReadLine(r, line)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error reading from file '%s'\nLine number %d\n", os.Args[1], line)
		os.Exit(1)
	}
	fmt.Println(s)
}
